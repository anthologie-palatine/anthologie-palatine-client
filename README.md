# Anthologie Palatine Client - v1.0

Client 100% statique (html-css-javascript) pour interagir avec l'API de l'Anthologie Palatine.
Une application [VueJS](https://vuejs.org/)

[**&rarr; Démo**](http://pop.anthologiegrecque.org/)

## Prérequis

Ce projet nécessite [Node.js](https://nodejs.org/fr/) `>=v8.0.0` et son compagnon `npm`.

Installer les outils Node.js :

```bash
npm install
```

## Production

```bash
npm run build
```

Les fichiers générés dans le dossier `dist/ ` (compressés/optimisés/etc.) sont alors prêts à déployer sur votre serveur de choix. 100% statique.

## Développement

Lancer un serveur de développement avec _hot-reload_ sur `http://localhost:8080` :

```bash
npm run start
```

Pour quitter, faites simplement `ctrl + c` dans le terminal.

### Arborescence de fichiers

- Tous les fichiers source sont dans le répertoire `src/`, à l'exception du fichier `index.html` (situé dans la racine du dépôt)
- Les composantes se situent dans le sous-dossier `components/`
- Généralement, chaque vue (ou route) possède sa propre composante (ex. `/auteurs` &rarr; `AuthorsComponent`)
- Les routes (`url`) sont gérées par le routeur dans `router`
- De plus petites composantes partagées se trouvent dans le sous-dossier `components/partials`
- Des fichiers statiques, tels que des images, se trouvent dans un autre dossier : `static`

### Configuration et opitmisation

L'application est bootstrappée avec [Webpack](https://webpack.js.org/).
Cela permet d'empaqueter les fichiers et de les optimiser suivant des pratiques (relativement…) standardisées.
Vous trouverez les fichiers de configuration dans le dossier racine `config`

## Conventions des branches

Adhérer aux principes de [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/).

### Nouvelles fonctionnalités

- **Branche de base :** `develop`
- **Nom de la branche :** `##-feature-<fonctionnalite>` (où `##` est le numéro du ticket associé)
- **Branche de fusion :** `develop`

Une fois les changements fusionnés (ne pas oublier de fusionner `--no-ff` pour conserver l'historique des commits), la branche de la fonctionnalité peut être supprimée.

### Nouvelles versions _(releases)_

- **Branche de base :** `develop`
- **Nom de la branche :** `##-release-<x.y>` (où `##` est le numéro du ticket associé)
- **Branches de fusion :** `develop` **et** `master`

Une fois les changements fusionnés (ne pas oublier de fusionner `--no-ff` pour conserver l'historique des commits), créer l'étiquette correspondante (`git tag <x.y>`) et supprimer la branche de la nouvelle version.

### Patchs

- **Branche de base :** `master`
- **Nom de la branche :** `##-hotfix-<x.y.z>` (où `##` est le numéro du ticket associé)
- **Branches de fusion :** `develop` **et** `master`

_Attention, ce sont des modifications rapides et ponctuelles!_

Une fois les changements fusionnés (ne pas oublier de fusionner `--no-ff` pour conserver l'historique des commits), créer l'étiquette correspondante (`git tag <x.y.z>`) et supprimer la branche de la nouvelle patch.
